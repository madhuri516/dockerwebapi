﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoWebAPIDockerProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        //Get /api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> GetValues()
        {
            return new String[] {"value1", "value2" };
        }

        //Get /api/values/5
        [HttpGet("{id}")]
        public ActionResult<int> GetValueId(int id)
        {
            return id;
        }

    }
}
