# DockerWebAPI
[![pipeline status](https://gitlab.com/madhuri516/dockerwebapi/badges/main/pipeline.svg)](https://gitlab.com/madhuri516/dockerwebapi/-/commits/main)


Overview
---------
This WebAPI service is a development project created solely for selection process of Butn client. It is a simple Web API application created in .Net framework 5.0 using .Net Core API and C#. The application intends to create 3 end points for end user to consume and fetch results. Below is a list of all the end points:

HelloWorld  
    GET: /api​/HelloWorld  
Maths  
Addition  
    GET: /api/Add  
Substraction  
    GET: ​/api​/Substract  
Values  
    GET: /api/Values  
    GET: /api/Values/id

The solution has been containerized using Docker and the image is pushed in Docker Hub.

Assumptions
------------
1. It has been assumed that a .Net web API application needs to be created as per requirement.

2. Only GET end points have been created for simplicity.

3. Https has not been configured to avoid certificate usage and related configurations for simplicity. Only Http on port 80 has been exposed.

4. Swagger has been used for OpenAPI specification.

How to Run
-----
Run below command on your local system:
docker run -d --rm -p 8080:80 madver/demo-webapi-docker-project

This will pull the image from Docker hub and start running the application.

API endpoints can be tested now either on browser or tools like Postman.
For e.g. http://localhost:8080/api/HelloWorld or http://localhost:8080/api/Values
or Swagger UI can also be viewed with following URL: http://localhost:8080/Swagger

CI/CD
------
CI/CD has been configured using GitLab. Shared Runners have been used for build and deployment purpose.
Following stages are used for building, testing and then pusblishing to image to docker hub.

-Build
- This stage is created to mainly build and test the docker image.
- Runs for all branches.

-Deploy
- Once the build stage is passed successfully, push the image to docker hub.
- Runs only for main branch.


