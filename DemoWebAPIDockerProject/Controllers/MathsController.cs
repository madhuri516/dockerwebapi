﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoWebAPIDockerProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MathsController : ControllerBase
    {
        //Get api/Maths/Add
        [HttpGet("Add")]
        public int Addition(int value1, int value2)
        {
            return value1 + value2;
        }

        //Get api/Maths/Substract
        [HttpGet("Substract")]
        public int Substraction(int value1, int value2)
        {
            if(value1 > value2)
            {
                return value1 - value2;
            }
            else
            {
                return value2 - value1;
            }
        }
    }
}
