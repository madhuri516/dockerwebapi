#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

# Get base SDK image from microsoft
FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80

# Copy the CSPROJ file and restore any dependencies , copy the project files and build our release
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["DemoWebAPIDockerProject/DemoWebAPIDockerProject.csproj", "DemoWebAPIDockerProject/"]
RUN dotnet restore "DemoWebAPIDockerProject/DemoWebAPIDockerProject.csproj"
COPY . .
WORKDIR "/src/DemoWebAPIDockerProject"
RUN dotnet build "DemoWebAPIDockerProject.csproj" -c Release -o /app/build

# Publish the build release
FROM build AS publish
RUN dotnet publish "DemoWebAPIDockerProject.csproj" -c Release -o /app/publish

# Generate finak runtime image
FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "DemoWebAPIDockerProject.dll"]